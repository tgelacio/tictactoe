===================================================================
                            
                            Tic-Tac-Toe                         

===================================================================

Overview
========
This is a simple android application that allows users to play a game of tic-tac-toe.
It is designed to work in both portrait and landscape orientations. 

This app is written using Java.

This app was initially coded in fulfillment of the requirements for the course
PROG3210: Programming Mobile Applications II.


Getting Started
===============

To run the project, follow these steps:

* Set up Android Studio 3.5.
* Create a virtual device with an API Level 20.
* Open the source code directory in Android Studio and proceed with 
  cleaning/building the application.
* After the project is built (which will take a while the very first time as
  dependencies need to be downloaded), simply run it via the virtual device.


Known Issues
============
There are no known issues while running on Android Studio 3.5. 


Why MIT License
================
For the license of this project, I chose to use an MIT license because it 
is simple and permissive. This license allows other users to modify the code 
but also requires preservation of copyright and license notices.

