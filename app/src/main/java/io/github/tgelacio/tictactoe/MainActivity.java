/*
TicTacToe Application
PROG3210 Assignment 1

Revision History
    Tonnicca Gelacio, 2019-09-24: Created
    Tonnicca Gelacio, 2019-09-25: Code Updated
    Tonnicca Gelacio, 2019-09-27: Code Updated
    Tonnicca Gelacio, 2019-09-28: Code Updated
    Tonnicca Gelacio, 2019-10-04: UI Updated
    Tonnicca Gelacio, 2019-10-05: Code and UI Updated

 */

package io.github.tgelacio.tictactoe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity
    implements View.OnClickListener {

    //Declarations - Constants
    public static final int NUMBER_OF_COLUMNS = 3;
    public static final int NUMBER_OF_ROWS = 3;
    public static final int MAX_MOVES = 9;

    // Declarations and Initializations - Global Variables
    Button aButtons[][] = new Button[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
    String gameBoardContent[][] = new String[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
    String winner;
    int movesCounter = 0;
    boolean winnerFound = false;
    boolean vacant = true;
    TextView playerTurnLabel;

    // Declaration - SharedPreference
    private SharedPreferences sharedPlace;

    ///
    /// CheckWinner() method
    /// To check if there's a winner after a player's turn
    ///
    public Boolean CheckWinner()
    {
        //Declarations and Initializations
        Boolean winnerFound;
        Boolean rowWinner = false;
        Boolean columnWinner = false;
        Boolean diagonalWinner = false;
        String[][] boardContent = gameBoardContent;

        //Check Rows
        for (int i = 0; i < boardContent.length; i++)
        {
            if ((boardContent[i][0] == boardContent [i][1]) &
                    (boardContent[i][1] == boardContent[i][2]) &
                    (boardContent[i][0].isEmpty() == false ))
            {
                rowWinner = true;
            }
        }

        //Check Columns
        for (int j=0; j < boardContent[0].length; j++)
        {
            if ((boardContent[0][j] == boardContent[1][j]) &
                    (boardContent[1][j] == boardContent[2][j]) &
                    (boardContent[0][j].isEmpty() == false))
            {
                columnWinner = true;
            }
        }

        //Check Diagonal
        if ((boardContent[0][0] == boardContent[1][1] &
        boardContent[1][1] == boardContent[2][2]) ||
        (boardContent[2][0] == boardContent[1][1] &
        boardContent[1][1] == boardContent[0][2]))
        {
            if (boardContent[1][1].isEmpty() == false)
            {
                diagonalWinner = true;
            }
        }

        if (rowWinner || columnWinner || diagonalWinner)
        {
            winnerFound = true;
        }

        else
        {
            winnerFound = false;
        }

        return winnerFound;
    }

    ///
    /// CheckVacant() method
    /// To check if there are remaining vacant tiles on the board
    ///
    public Boolean CheckVacant()
    {
        //Declarations
        int vacantCount;
        Boolean vacant;
        String[][] boardContent = gameBoardContent;

        //Initializations
        vacantCount = 0;
        vacant = true;

        //Check for vacant slots
        for (int i = 0; i < boardContent.length; i++)
        {
            for (int j=0; j < boardContent[0].length; j++)
            {
                if (boardContent[i][j].isEmpty())
                {
                    vacantCount++;
                }
            }
        }

        if (vacantCount > 0)
        {
            vacant = true;
        }

        else
        {
            vacant = false;
        }

        return vacant;
    }

    ///
    /// NewGame() method
    /// To re-initialize the game board and the labels once players'
    /// decide to start a new game.
    ///
    public void NewGame()
    {
        for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
        {
            for(int i = 0; i < NUMBER_OF_ROWS; i++)
            {
                aButtons[i][j].setText("");
                aButtons[i][j].setClickable(true);
                gameBoardContent[i][j] = "";
                playerTurnLabel.setText("Player X starts.");
            }
        }

        // set movesCounter to zero
        movesCounter = 0;
        winnerFound = false;
    }

    ///
    /// GameFinished() method
    /// To disable the tiles on the board once a game ends.
    ///
    public void GameFinished()
    {
        for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
        {
            for(int i = 0; i < NUMBER_OF_ROWS; i++)
            {
                // Make buttons unclickable once game ends
                aButtons[i][j].setClickable(false);
            }
        }
    }

    ///
    /// Event handler of onCreate method
    ///
    ///
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize attributes from the form
        playerTurnLabel =  findViewById(R.id.playerTurnLabel);
        aButtons[0][0] = findViewById(R.id.button00);
        aButtons[0][1] = findViewById(R.id.button01);
        aButtons[0][2] = findViewById(R.id.button02);
        aButtons[1][0] = findViewById(R.id.button10);
        aButtons[1][1] = findViewById(R.id.button11);
        aButtons[1][2] = findViewById(R.id.button12);
        aButtons[2][0] = findViewById(R.id.button20);
        aButtons[2][1] = findViewById(R.id.button21);
        aButtons[2][2] = findViewById(R.id.button22);

        // Initialize SharedPreferences
        this.sharedPlace = getSharedPreferences("SharedPlace", MODE_PRIVATE);

        // Initialize arrays
        for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
        {
            for(int i = 0; i < NUMBER_OF_ROWS; i++)
            {
                // Set click event handler
                aButtons[i][j].setOnClickListener(this);

                aButtons[i][j].setText("");
                gameBoardContent[i][j] = "";
            }
        }

        // Set text
       playerTurnLabel.setText("Player X starts.");
    }

    ///
    /// Event handler of onCreateOptionsMenu method
    ///
    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tictactoe_menu, menu);
        return true;
    }

    ///
    /// Event handler of onOptionsItemSelected method
    ///
    ///
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.mnuNewGame:
                NewGame();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    ///
    /// Event handler of onPause method
    ///
    ///
    @Override
    protected void onPause() {

        // Declarations
        String name = "";
        String value = "";

        SharedPreferences.Editor sharedEditor = this.sharedPlace.edit();

        // Save current array values to sharedEditor
        for (int i = 0; i < NUMBER_OF_ROWS; i++)
        {
            for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
            {
                name = "button" + i + j;
                value = gameBoardContent[i][j];
                sharedEditor.putString(name, value);
            }
        }

        sharedEditor.putBoolean("winnerFound", winnerFound);
        sharedEditor.putInt("movesCounter", movesCounter);
        sharedEditor.putString("currentTurn", playerTurnLabel.getText().toString());
        sharedEditor.commit();

        super.onPause();
    }

    ///
    /// Event handler of onResume method
    ///
    ///
    @Override
    protected void onResume() {
        super.onResume();

        // Declarations
        String name = "";
        String value = "";

        winnerFound = this.sharedPlace.getBoolean("winnerFound", winnerFound);
        movesCounter = this.sharedPlace.getInt("movesCounter", movesCounter);

        for (int i = 0; i < NUMBER_OF_ROWS; i++)
        {
            for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
            {
                name = "button" + i + j;
                gameBoardContent[i][j] = this.sharedPlace.getString(name, "");
                aButtons[i][j].setText(gameBoardContent[i][j]);
            }
        }

        playerTurnLabel.setText(this.sharedPlace.getString("currentTurn", ""));

    }

    ///
    /// Event handler of onSaveInstanceState method
    ///
    ///
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // Declarations
        String name = "";
        String value = "";

        // Save current array values
        for (int i = 0; i < NUMBER_OF_ROWS; i++)
        {
            for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
            {
                name = "button" + i + j;
                value = gameBoardContent[i][j];
                outState.putString(name, value);
            }
        }

        outState.putBoolean("winnerFound", winnerFound);
        outState.putInt("movesCounter", movesCounter);
        outState.putString("currentTurn", playerTurnLabel.getText().toString());
    }

    ///
    /// Event handler of onRestoreInstanceState method
    ///
    ///
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Declarations
        String name = "";

        winnerFound = savedInstanceState.getBoolean("winnerFound");
        movesCounter = savedInstanceState.getInt("movesCounter");

        for (int i = 0; i < NUMBER_OF_ROWS; i++)
        {
            for (int j = 0; j < NUMBER_OF_COLUMNS; j++)
            {
                name = "button" + i + j;
                gameBoardContent[i][j] = savedInstanceState.getString(name);
                aButtons[i][j].setText(gameBoardContent[i][j]);
            }
        }
        playerTurnLabel.setText(savedInstanceState.getString("currentTurn"));
    }

    ///
    /// Click event handler of the buttons
    ///
    ///
    @Override
    public void onClick(View v)
    {
        Button selectedButton = (Button) v;
        String buttonValue = (String) selectedButton.getText();

        // Check if selected tile is empty
        if (buttonValue.isEmpty())
        {
            movesCounter++;

            // Change button's text to X or O
            if (movesCounter%2 != 0)
            {
                selectedButton.setText("X");
                playerTurnLabel.setText("Player O's turn.");
            }

            else
            {
                selectedButton.setText("O");
                playerTurnLabel.setText("Player X's turn.");
            }

            // update boardContent
            switch (selectedButton.getId()) {
                case R.id.button00:
                    gameBoardContent[0][0] = selectedButton.getText().toString();
                    break;
                case R.id.button01:
                    gameBoardContent[0][1] = selectedButton.getText().toString();
                    break;
                case R.id.button02:
                    gameBoardContent[0][2] = selectedButton.getText().toString();
                    break;
                case R.id.button10:
                    gameBoardContent[1][0] = selectedButton.getText().toString();
                    break;
                case R.id.button11:
                    gameBoardContent[1][1] = selectedButton.getText().toString();
                    break;
                case R.id.button12:
                    gameBoardContent[1][2] = selectedButton.getText().toString();
                    break;
                case R.id.button20:
                    gameBoardContent[2][0] = selectedButton.getText().toString();
                    break;
                case R.id.button21:
                    gameBoardContent[2][1] = selectedButton.getText().toString();
                    break;
                case R.id.button22:
                    gameBoardContent[2][2] = selectedButton.getText().toString();
                    break;
                default:
                    Toast.makeText(this, "Default switch onClick error.",  Toast.LENGTH_LONG).show();
                    break;
            }

            // Check for winner
            if ((movesCounter >= 5) & (movesCounter <= MAX_MOVES))
            {
                winnerFound = CheckWinner();
                vacant = CheckVacant();

                if (winnerFound)
                {
                    winner = selectedButton.getText().toString();
                    playerTurnLabel.setText(winner + " wins!");
                    GameFinished();
                }

                else
                {
                    if (vacant == false)
                    {
                        playerTurnLabel.setText("Draw.");
                        GameFinished();
                    }
                }
            }

        }
    }

}
